const express = require('express');
const session = require('express-session');
const FirebaseStore = require('connect-session-firebase')(session);
var admin = require("firebase-admin");

var serviceAccount = require("../secretKeys/bookstore-e01ca-firebase-adminsdk.js");

const ref = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://bookstore-e01ca.firebaseio.com"
});
module.exports = {
    store: new FirebaseStore({
        database: ref.database()
    })
}