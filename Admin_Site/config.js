const bcrypt = require('bcrypt');
module.exports = {
    port : 4000,
    sessionSecretKey : bcrypt.hashSync("SECRET_KEY", 2),
    dbname : 'dbname',
    sessionKeys : ['key1','key2'],
    MONGODB_URL  :'mongodb://BookStoreDeveloper:asd098.zxc@bookstorecluster-shard-00-00-awhau.mongodb.net:27017,bookstorecluster-shard-00-01-awhau.mongodb.net:27017,bookstorecluster-shard-00-02-awhau.mongodb.net:27017/test?ssl=true&replicaSet=BookStoreCluster-shard-0&authSource=admin&retryWrites=true', //mongodb://localhost/${dbname}
    productImageSavingLocation : '../public/images/product-img/',
    websiteImageSavingLocation : '../public/images/website-img/'
}